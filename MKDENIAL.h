#ifndef __MKDENIAL_CONSTS_H__
#define __MKDENIAL_CONSTS_H__

#include <include/EcommerceOptions.h>

const int MKDENIAL_ID = 34;
const int CONTROL_PARAMETERID = 34;

#include "curlclient.h"
#include "xmlpacket.h"

#define strPlgName "\"MKDENIAL plugin\""
#define ANSPATTERN "successful"
#endif
