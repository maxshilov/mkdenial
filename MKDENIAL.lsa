parent "BM/BM.lsa"
embed "MKDENIAL.type"
Class MKDenialConf "MKDenial Connection Settings" {

  Attributes {
    "Connection Settings":
    BM::Account   ,NOTAB, PK;
    STR127        Url, NOT NULL "Server URL";    
    Virtual STR Answer;
    STR256        HealthUrl, NOT NULL "Health check URL";
  }

  Methods {

   const EPluginConfGet( - BM :: Processing, BM :: Account )
        dispatched by BM::Processing.ProcessingConfGet
        return - BM :: Processing, BM :: Account, Url, HealthUrl;
    Get( BM :: Account )
        return BM :: Account, Url, HealthUrl;



    // standard functions to manage plugin
    ActivatePlugin();
    DeactivatePlugin();
    TestConnection();
	
	
	
    // plugin interface
    const GetData( BM::FraudScreeningInterface.PluginID )
      dispatched by BM::FraudScreeningPlugin.GetPluginProperties
      return BM::FraudScreeningInterface.PluginID,
        BM::FraudScreeningInterface.PluginName,
        BM::FraudScreeningInterface.Description,		
        BM::FraudScreeningInterface.ControlParameterName,
        BM::FraudScreeningInterface.ContainsBlackList,
        BM::FraudScreeningInterface.IsActive,
		Url;
      /* Gets Plugin data and redirects to the MKDenial Payment Method Validation Win */

    const GetAnswers( BM::ControlParameter.ControlParameterID )
      dispatched by BM::FraudScreeningInterface.GetAnswers;
      /* Provides the List of Plugin's Answers */

    const CheckAnswer( BM::FraudScreeningInterface.Answer ) return BM::FraudScreeningInterface.IsAnswerValid;
      /* Checks, whether Vendor has chosen an Answer from the List */

    const CheckCondition( BM::FraudScreeningInterface.FraudCheckID, BM::FraudScreeningInterface.ConditionID )
      dispatched by BM::FraudScreeningInterface.CheckCondition
      return BM::FraudScreeningInterface.PluginMessage,
        BM::FraudScreeningInterface.RevealedParamValue,
        BM::FraudScreeningInterface.BlackListStatus,
        BM::FraudScreeningInterface.CheckResult;
      /* Fraud Checking function */

    MKDenialConfMyUpdate( *Url, *HealthUrl);
    SaveEditData( *Url, *HealthUrl);
	
    
    const MKDenialConfMyGet()
      dispatched by BM::Processing.ProcessingConfGet, MKDENIAL::MKDenialConf.EPluginConfGet
      return -BM::Account, Url, HealthUrl;
    const MKDenialConfMyGetForUpdate()
      dispatched by BM::Processing.ProcessingConfGet, MKDENIAL::MKDenialConf.EPluginConfGet
      return -BM::Account, Url, HealthUrl;
    const GetInfoForEdit(-BM::Account)
      dispatched by BM::Processing.ProcessingConfGet, MKDENIAL::MKDenialConf.EPluginConfGet
      return -BM::Account, Url, HealthUrl;
    

   

  }

  Views {

    Window GetWin "MKDenial System Configuration" {
      StandardInput : EPluginConfGet;
      Update( "Edit" ) : EditWin;
    }


    Window ConfigureWin "Configure MKDenial Connection Settings" {
    StandardInput: MKDenialConfMyGetForUpdate;    
     Save("Save") : MKDenialConfMyUpdate;
    
    }
	
    Window EditWin "Update Fraud Connection Settings" {
      StandardInput: GetInfoForEdit;
      Save("Save") : MKDenialConfMyUpdate;
    }
    
    DataWindow AvailableAnswersList "Available Answers" {
      Answer;
      StandardInput : GetAnswers;
    } return Answer;

    // MAIN WINDOW
    Window MKDenialConfMainWin "Plugin 'MKDenial  Validation'" {
      StandardInput: GetData;
      activate("Activate") : ActivatePlugin;
      deactivate("Deactivate") : DeactivatePlugin;
	  Update("Edit") : MKDenialConf.ConfigureWin;
      testconn("Test_Connection") : TestConnection;
    }
  }
}



Groups {
  // Order Processing
  ORDERS_VIEW      1101;
  ORDERS_CREATE    1102;
  ORDERS_PROCESS   1103;
  ARDOCS_VIEW      1104;
  ARDOCS_CREATE    1105;
  PAYMENTS_PROCESS 1106;

  // Accounts
  ACCOUNTS_VIEW_DET       1110;
  ACCOUNTS_CREATE_MODIFY  1111;

  // Financial Management
  PAYMENT_METHODS_CREATE_MODIFY  1120;
  KEY_CUSTODIAN 1122;

  // Reports
  REPORTS_VIEW     1130;

  // Product management
  PRODUCT_MANAG_CONFIGURE   1140;
  PRODUCT_MANAG_VIEW_PLANS  1141;
  PRODUCT_MANAG_MODIF_PLANS 1142;
  PRODUCT_MANAG_MARKETING   1143;

  // Provisioning
  PROVISIONING_GENERAL      1150;
  PROVISIONING_MANUAL_OPS   1151;

  // Communications
  COMMUNIC_VIEW_MSG_LOG     1160;
  COMMUNIC_MODIFY_SETTINGS  1161;
  COMMUNIC_MODIFY_TEMPLATES 1162;

  // Configuration
  CONFIG_EXTERNAL_SYSTEMS   1170;
  CONFIG_ECOMMERCE          1171;
  CONFIG_ORDER_PROCESSING   1172;
  CONFIG_FRAUD_SCREENING    1173;
  CONFIG_SALES_FIN_SETINGS  1174;
  CONFIG_MISC_ORDER_NUMB    1175;
  CONFIG_SECURITY           1176;
  CONFIG_TTS                1177;
  CONFIG_STORE              1178;

  CONFIG_GLOBAL_SETTINGS    1180;

  // Support
  PROCESS_TTS               1190;
  PROVIDE_SUPPORT           1191;

  // Customer
  MANAGE_OWN_ACCTINFO       1200;
  MANAGE_OWN_SUBSCRS        1201;
  MANAGE_OWN_DOMAINS        1202;
  MANAGE_OWN_FIN_INFO       1203;

  CUST_MAKE_PURCHASES       1205;

  MANAGE_OWN_ACCTINFO_C     1210;
  MANAGE_OWN_SUBSCRS_C      1211;
  MANAGE_OWN_DOMAINS_C      1212;
  MANAGE_OWN_FIN_INFO_C     1213;

  // Reseller
  MANAGE_RESELLERS_ORDERS   1220;

 
  NOBODY 666; // Unknown user. Used by Password reminder.
  AUDIT 700; // Can view history of visible screens

 

  MKDenialConf access granted
    audit AUDIT
    writable CONFIG_FRAUD_SCREENING CONFIG_GLOBAL_SETTINGS CONFIG_ECOMMERCE
    readonly PROVIDE_SUPPORT
  {
    CheckCondition granted all;
    ActivatePlugin denied all;
    ActivatePlugin granted CONFIG_GLOBAL_SETTINGS;
	TestConnection denied all;
    TestConnection granted CONFIG_GLOBAL_SETTINGS;
 


    DeactivatePlugin denied all;
    DeactivatePlugin granted CONFIG_GLOBAL_SETTINGS;
  }

  
}
