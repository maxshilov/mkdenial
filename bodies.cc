#include <iMKDENIAL.h>

// plugin
#include <internals/oMKDenialConf.hpp>
// core
#include <internals/oARDoc.hpp>
#include <internals/oAccount.hpp>
#include <internals/oCardClass.hpp>
#include <internals/oCardProcessing.hpp>
#include <internals/oCreditCard.hpp>
#include <internals/oEActivity.hpp>
#include <internals/oPayTool.hpp>
#include <internals/oPayment.hpp>
#include <internals/oFraudScreeningPlugin.hpp>
#include <internals/oFraudScreeningFilter.hpp>
#include <internals/oFraudScreeningRule.hpp>
#include <internals/oFraudCheck.hpp>
#include <internals/oRuleCondition.hpp>
#include <internals/oControlParameter.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include "include/PbaHelper.h"




ItemResult* MKDENIAL :: EPluginConfGet(Int PluginID, Int AccountID) {
    TRACE_CALL_ARGS(log(), PluginID << AccountID);
    if (AccountID != oAccount.getCurrID()) {
        throw Exc("Access denied. ");
    }
    MKDenialConf tc = oMKDenialConf.getEntry(AccountID, ORDBMS::FOR_UPDATE);
    ItemResult* r = new ItemResult();

    r->AppendValue(AccountID, AC_CODE);
    if (oMKDenialConf.checkID(AccountID, ORDBMS::FOR_UPDATE)) {
        r->AppendValue(tc.Url, AC_LTEXT);
        r->AppendValue(tc.HealthUrl, AC_LTEXT);
    } else {
        r->AppendValue(STR_NULL, AC_LTEXT);
        r->AppendValue(STR_NULL, AC_LTEXT);
    }

    r->SetWindowID("MKDENIAL.MKDenialConf_ConfigureWin");

    return r;
}



ItemResult* MKDENIAL :: MKDenialConfMyGet() {

    return MKDENIAL::MKDenialConfMyGetForUpdate( );

}

ItemResult* MKDENIAL :: MKDenialConfMyGetForUpdate() {
    ItemResult* r = new ItemResult();
    Int accID = oAccount.getCurrID();
    TRACE_CALL_ARGS(log(),  accID );
    r->AppendValue(accID, AC_CODE);

    if (oMKDenialConf.checkID(accID, ORDBMS::FOR_UPDATE)) {
        MKDenialConf tc = oMKDenialConf.getEntry(accID, ORDBMS::FOR_UPDATE);
        r->AppendValue(tc.Url, AC_LTEXT);
        r->AppendValue(tc.HealthUrl, AC_LTEXT);
    } else {
        r->AppendValue(STR_NULL, AC_LTEXT);
        r->AppendValue(STR_NULL, AC_LTEXT);
    }
    return r;
}


ItemResult* MKDENIAL :: GetInfoForEdit( Int accID ) {
    ItemResult* r = new ItemResult();

    TRACE_CALL_ARGS(log(),  accID );
    r->AppendValue(accID, AC_CODE);

    if (oMKDenialConf.checkID(accID, ORDBMS::FOR_UPDATE)) {
        MKDenialConf tc = oMKDenialConf.getEntry(accID, ORDBMS::FOR_UPDATE);
        r->AppendValue(tc.Url, AC_LTEXT);
        r->AppendValue(tc.HealthUrl, AC_LTEXT);
    } else {
        r->AppendValue(STR_NULL, AC_LTEXT);
        r->AppendValue(STR_NULL, AC_LTEXT);
    }

    r->SetWindowID("MKDENIAL.MKDenialConf_EditWin");
    return r;
}



ErrorMessage* MKDENIAL :: MKDenialConfMyUpdate(Str Url, Str HealthUrl) {
    TRACE_CALL_ARGS(log(),  Url);

    Int accID = oAccount.getCurrID();

    if (oMKDenialConf.checkID(accID, ORDBMS::FOR_UPDATE)) {
        oMKDenialConf.updateEntry(accID, Url, HealthUrl);
    }
    else{
        oMKDenialConf.addEntry(accID, Url, HealthUrl);
    }

    return NULL;
}


ErrorMessage* MKDENIAL :: SaveEditData(Str Url, Str HealthUrl) {
    TRACE_CALL_ARGS(log(),  Url);

    return NULL;
}

ErrorMessage* MKDENIAL :: ActivatePlugin() {
    TRACE_CALL(log(), "");

    FraudScreeningPlugin plugin = oFraudScreeningPlugin.getEntry(MKDENIAL_ID, ORDBMS::FOR_UPDATE);

    plugin.IsActive = BOOL_YES;
    oFraudScreeningPlugin.updateEntry(plugin);

    return new ErrorMessage(Msg("The plug-in ") << Msg(strPlgName).what() << " is activated.");
}

ErrorMessage* MKDENIAL :: DeactivatePlugin() {
    TRACE_CALL(log(), "");

    FraudScreeningPlugin plugin = oFraudScreeningPlugin.getEntry(MKDENIAL_ID, ORDBMS::FOR_UPDATE);

    plugin.IsActive = BOOL_NO;
    oFraudScreeningPlugin.updateEntry(plugin);

    return new ErrorMessage(Msg("The plug-in ") << Msg(strPlgName).what() << " is deactivated. ");
}

ErrorMessage* MKDENIAL :: TestConnection() {
    CurlClient cClient;
    Str sMsg = "Everything is OK!";
    MKDenialConf tc = oMKDenialConf.getEntry( oAccount.getCurrID(), ORDBMS::READ_ONLY);
    //timeouts in seconds
    cClient.setTimeOut(60); cClient.setConnectTimeOut(30);
    cClient.setUrl(tc.HealthUrl.c_str());
    Str sResponse = cClient.callGet();
    log() << "HealthCheck response: " << sResponse << endl;
    //connection errors and timeouts
    if ( cClient.getErrorCode() != 0 ){
        sMsg = STLRT::Util::stringify( cClient.getErrorCode()) << ":" << cClient.getErrorBuffer();
        throw Exc(sMsg.c_str());
    }

    if (sResponse.findStr("EVERYTHINGISOKAY") == Str::npos)
        throw Exc("Something went wrong. Please check PBA logs.");

    return new ErrorMessage( Msg(sMsg.c_str()) );
}


ItemResult* MKDENIAL :: GetData(Int PluginID) {
    TRACE_CALL_ARGS(log(), PluginID);

    Int accID = oAccount.getCurrID();
    ItemResult* r = new ItemResult();
    if (PluginID != MKDENIAL_ID) {
        throw Exc("Misconfiguration detected: ") << Msg(strPlgName).what() << " is installed with wrong plug-in ID (" << PluginID << "). ";
    }

    ControlParameter param = oControlParameter.getEntry(CONTROL_PARAMETERID, ORDBMS::READ_ONLY);
    FraudScreeningPlugin plugin = oFraudScreeningPlugin.getEntry(MKDENIAL_ID, ORDBMS::READ_ONLY);

    r->AppendValue((Int)(plugin.fraudScreeningPluginID.value()), AC_CODE, "ID");
    r->AppendValue((Str)(plugin.name.value()), AC_LTEXT, "Plugin Name");
    r->AppendValue((Str)(plugin.Description.value()), AC_LTEXT, "Description");
    r->AppendValue((Str)(param.ControlParameterName.value()), AC_LTEXT, "Parameter");
    r->AppendValue((Int)(plugin.ContainsBlackList.value()), AC_CODE, "Contains Black List");
    r->AppendValue((Int)(plugin.IsActive.value()), AC_CODE, "Is Active");
    if (oMKDenialConf.checkID(accID, ORDBMS::FOR_UPDATE)) {
        MKDenialConf tc = oMKDenialConf.getEntry(accID, ORDBMS::FOR_UPDATE);
        r->AppendValue(tc.Url, AC_LTEXT);
        r->AppendValue(tc.HealthUrl, AC_LTEXT);
    } else {
        r->AppendValue(STR_NULL, AC_LTEXT);
        r->AppendValue(STR_NULL, AC_LTEXT);
    }

    r->SetWindowID("MKDENIAL.MKDenialConf_MKDenialConfMainWin");

    return r;

}

ListResult* MKDENIAL :: GetAnswers(Int ControlParameterID, Int SortNo, Str) {
    TRACE_CALL_ARGS(log(), ControlParameterID);


    ListResult* lr = new ListResult("Answers list");

    lr->AddColumn("Answer", AC_LTEXT);
    lr->AppendValues(lr->rout() << ANSPATTERN);
    lr->Sort(SortNo);

    return lr;
}

ItemResult* MKDENIAL :: CheckAnswer(Str Answer) {
    TRACE_CALL_ARGS(log(), Answer);
    ItemResult* ir = new ItemResult();

    if (Answer == ANSPATTERN)
        ir->AppendValue(BOOL_YES, AC_CODE);
    else
        ir->AppendValue(BOOL_NO, AC_CODE);

    return ir;
}

inline Str CheckFraud( MKDENIAL* ths, Str strUrl, Str strRef, Str strCountry, Str strCity, Str strName, Str strAddress1, Str strAddress2){

    StrStream request("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    request << Str()+"\n\
    <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:fra=\"http://softcom.com/fraud.webservices.im.com\">\n\
     <soapenv:Header/>\n\
     <soapenv:Body>\n\
      <fra:API_MKDenial>\n\
         <fra:strReference>" << strRef << "</fra:strReference>\n\
         <fra:strCountry>"<< strCountry <<"</fra:strCountry>\n\
         <fra:strCity>"<< strCity <<"</fra:strCity>\n\
         <fra:strName>"<< strName <<"</fra:strName>\n\
         <fra:strAddr>"<< strAddress1 <<"</fra:strAddr>\n\
         <fra:strAddr2>"<< strAddress2 <<"</fra:strAddr2>\n\
      </fra:API_MKDenial>\n\
     </soapenv:Body>\n\
    </soapenv:Envelope>";

    ths->note() << "MKDenial Url :" << strUrl<< " request: " << request << endl ;	
    if (strCountry.IsEmpty() || strCity.IsEmpty() || strAddress1.IsEmpty() || strName.IsEmpty()){
    	ths->note() << "Empty data provided. Skipping check...";
        return "false";
    }

    CurlClient cc;
    cc.setUrl( strUrl.c_str() );
    cc.setContentType("text/xml");

    std::string xmlResponse = cc.callPost( request.c_str() );

    if ( cc.getErrorCode() != 0 ) {
        const Str errCode = STLRT::Util::stringify( cc.getErrorCode() );
        throw Exc("Error connecting to webservice ") << errCode << " : " << cc.getErrorBuffer();
    }
    ths->note() << "MKDenial response: " << xmlResponse << endl;
    XMLPacket xml( xmlResponse );

    Str error = xml.executeXPathStr("string(/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='Fault']/*[local-name()='faultstring']/text())");
    if ( error.IsNotEmpty() ) {
      	ths->note() << "Got fault response from webservice: " << error << endl;
        throw Exc(error.c_str());
    }

    Str response = xml.executeXPathStr("string(/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='API_MKDenialResponse']/*[local-name()='API_MKDenialResult']/text())");
    ths->note() << "MKDenial fraud check result : " << response << endl;
    return response;
}

ItemResult* MKDENIAL :: CheckCondition(Int FraudCheckID, Int ConditionID) {
    TRACE_CALL_ARGS(log(), FraudCheckID << ConditionID);
    Str strName;
    Int result = AR_ERROR;

    RuleCondition condition = oRuleCondition.getEntry(ConditionID, ORDBMS::READ_ONLY);
    FraudCheck fc = oFraudCheck.getEntry(FraudCheckID, ORDBMS::READ_ONLY);

    if (fc.DocID.IsNull()) throw Exc("Payment is not found. ");

    const Int chkres = (condition.QuestionType.value() == FQT_POSITIVE ? FRCR_TRUE : FRCR_FALSE);
    Payment payment = oPayment.getEntry(fc.DocID, ORDBMS::READ_ONLY);
    ARDoc arp = oARDoc.getEntry(fc.DocID, ORDBMS::READ_ONLY);
    PayTool payTool = oPayTool.getEntry(payment.PayToolID, ORDBMS::READ_ONLY);
    PayToolData ptdata = oPayTool.getPayTool(payment.PayToolID);
    const Int accID = arp.Vendor_AccountID.value(); // Vendor
    Account acc  = oAccount.getEntry(arp.Customer_AccountID.value(),ORDBMS::READ_ONLY);

    MKDenialConf tc = oMKDenialConf.getEntry(accID, ORDBMS::FOR_UPDATE);

    bool isFraud;
    //string Url = "http://209.41.154.105/fraud/FraudWS.asmx?op=API_MKDenial";
    Str adminName, billName;
    if( acc.TaxStatus == TAXSTATUS_PERSON ) {
        adminName = acc.CompanyName;
        billName = ptdata.creditCard.CardHolderName;
    }else{
        // as in myhosting : CompanyName + " " + John V. Smith
        adminName = Str() << acc.CompanyName << Str(" ")
                    << (acc.AdminFName.value().IsEmpty() ? Str(" ") : acc.AdminFName)
                    << Str(" ")
                    << (acc.AdminMName.value().IsEmpty() ? Str(" ") : acc.AdminMName)
                    << Str(" ")
                    << (acc.AdminLName.value().IsEmpty() ? Str(" ") : acc.AdminLName);

        billName = Str() << acc.CompanyName << Str(" ") << ptdata.creditCard.CardHolderName;
	}

    note() << "MKDenial Check Fraud for #account " << acc.AccountID<< endl;
    try{
	Str email = acc.AdminEmail.value().IsEmpty() ? acc.BillEmail: acc.AdminEmail;
        email = email.IsEmpty() ? acc.PersEmail : email;
        
	Str adminRes = CheckFraud(this, tc.Url, email,
                                acc.CountryID.value().trim().toUpper(),
                                acc.City,
                                adminName,
                                acc.Address1, acc.Address2);

        Str billRes = CheckFraud(this, tc.Url, email,
                                 ptdata.payTool.CountryID.value().trim().toUpper(),
                                 ptdata.payTool.City,
                                 billName,
                                 ptdata.payTool.Address1, ptdata.payTool.Address2);

        if(adminRes=="true" || billRes == "true")
            isFraud = true;
    }catch(Exc& ex){
        throw Exc( "Error in parsing request =  " ) << ex.what();
    }

    ItemResult* ir = new ItemResult();
    if(isFraud){
        ir->AppendValue(Str("result is ") << condition.Answer << ". ", AC_LTEXT);
        ir->AppendValue(Str(), AC_LTEXT);
        ir->AppendValue(FBLS_NA, AC_CODE);
        ir->AppendValue(chkres, AC_CODE);
    }
    else{
        ir->AppendValue(Str("result is ") << condition.Answer << ". ", AC_LTEXT);
        ir->AppendValue(Str(), AC_LTEXT);
        ir->AppendValue(FBLS_NA, AC_CODE);
        ir->AppendValue(!chkres, AC_CODE);
    }

    return ir;
}

