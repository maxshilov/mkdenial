
# Fraud Screening Plug-in
db::do_identity("
  INSERT INTO `FraudScreeningPlugin`(`fraudScreeningPluginID`, `name`, `ContName`, `refundUsable`, `registerUsable`, `Description`, `ContainsBlackList`, `IsActive`, `UserArc`, `DateArc` )
    VALUES ( 34, 'MKDenial plugin', 'MKDENIAL', 1, 1, 'Allows to check .',
 0, 1, 1, 0);", "FraudScreeningPlugin");

db::do_identity("
INSERT INTO `ControlParameter` (`ControlParameterID`, `fraudScreeningPluginID`, `ControlParameterName`, `Description`, `QuestionPart`, `UserArc`, `DateArc`)
  VALUES(34, 34, 'MKDenial plugin', 'MKDenial plugin', 'is', 1, 1);
", "ControlParameter");

db::sql_do("INSERT INTO `FraudScreeningRule`(`AccountID`, `Name`, `Type`, `Importance`, `UsePluginMessages`, `FulfillMessage`, `FailMessage`, `TriggerOn`, `ExcOnOrderPlacement`, `Copied_From_RuleID`, `UserArc`, `DateArc`)
  VALUES(1,'MKDenial plugin',300,0,1,NULL,NULL,25,NULL,NULL,1,1)");

my $query = db::query("Select max(`RuleID`) from `FraudScreeningRule`");
my @row = $query->fetchrow_array();
my $new_RuleID = $row[0];

db::sql_do("INSERT INTO `RuleCondition`(`RuleID`, `ControlParameterID`, `QuestionType`, `Answer`, `UserArc`, `DateArc`) VALUES(".$new_RuleID.",34,10,'successful',1,1)");

# Before order placement
my $query1 = db::query("SELECT * FROM `FraudScreeningFilter` WHERE `FilterID`=2 AND `Name`='Filter for Checking Orders on Placement from Store'");

if( my @row = $query1->fetchrow_array() ) {
  $query1->finish();
  db::sql_do("INSERT INTO `RuleInFraudFilter` (`FilterID`, `RuleID`, `Applicable`, `OrderForSort`, `UserArc`, `DateArc`) VALUES(2, ".$new_RuleID.", 0, 1, 1, 1)");
}

# After order placement
my $query2 = db::query("SELECT * FROM `FraudScreeningFilter` WHERE `FilterID`=1 AND `Name`='Filter for Checking Orders after Placement'");

if( my @row = $query2->fetchrow_array() ) {
  $query2->finish();
  db::sql_do("INSERT INTO `RuleInFraudFilter` (`FilterID`, `RuleID`, `Applicable`, `OrderForSort`, `UserArc`, `DateArc`) VALUES(1, ".$new_RuleID.", 0, 1, 1, 1)");
}

# Processing Center
db::do_identity("
  INSERT INTO `Processing` (`PluginID`, `Name`, `Container`, `State`, `Type`, `UserArc`, `DateArc`) VALUES ( 234, 'MKDenial Processing Center', 'MKDENIAL', 1, 0, 1, 0);
", "Processing");



pba::do_die "task_handler.pl";


sql_source("table.sql");
sql_source("trigger.sql");

return 1;
