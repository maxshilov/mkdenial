Name:           bm-payment-plugin-%{lwcontainer}
Version:	%{version}
Release:	%{release}
Summary:        Parallels Business Automation (PBA) custom %{title}.
License:        Commercial
Group:          System Environment/Applications
Prefix:         /usr/local/bm
Buildroot:	/tmp/%{name}-%{version}-root


%description
Parallels Business Automation (PBA) custom %{title}.

%define _linux 1
%define stdusr	pba
%define stdgrp	%{stdusr}

%include %{prefix}/share/bm.files.defs

%post
/sbin/ldconfig %{_libdir}


%postun
if [ $1 = 0 ] ; then
  /sbin/ldconfig
fi

%files
%{_ssmconfdir}/containers-list.dist.%{container}
%include %{prefix}/share/std-container.files

