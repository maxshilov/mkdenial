#! /usr/bin/env python
# encoding: utf-8

import os, sys, shutil, re

from __main__ import set_platform_syspath
from __main__ import BUILDROOT
set_platform_syspath()

def add_contrib(name):
	root = None
	if name.upper() + '_ROOT' in os.environ:
		root = os.environ[name.upper() + '_ROOT']
	elif BUILDROOT:
		root = os.path.join(BUILDROOT, 'usr', 'local', name)
	elif os.path.exists('/usr/local/' + name):
		root = '/usr/local/' + name
	if not root or not os.path.exists(root):
		raise ImportWarning("Unable to determine %s root." % name)

	contrib = os.path.join(root, 'share', 'waftools')
	if not os.path.exists(contrib):
		raise ImportWarning("Unable to find %s contributed Waf classes." % name)

	sys.path[0:0] = [ contrib ]
	return root

stellart_root	= add_contrib('stellart')
pba_root	= add_contrib('bm')

import Options

import LISA
import stellart as stlrt

import pbahelper

# the following two variables are used by the target "waf dist"
APPTITLE='MKDenial Ecommerce Plug-in'
VERSION='1.0.0'
RELEASE=os.getenv('RELEASE', '001')
APPNAME='MKDENIAL'

# these variables are mandatory ('/' are converted automatically)
srcdir = '.'
blddir = '.build'


def set_options(opt):
	opt.tool_options('stellart')

	gr = opt.add_option_group("PBA build configuration")
	gr.add_option('-r', '--rpm',
		action  = 'store_true',
		help    = 'build an RPM package',
	)

	pass


def configure(conf):
	# Prevent possible race conditions on parallel running just compiled check programs
	Options.options.jobs = 1
	# Set installation root by default
	Options.options.installroot = os.path.abspath('build')
	# Specify detected root for PBA SDK
	conf.env.PBASDK_ROOT = pba_root
	conf.env.PBA_ECOMMERCE_SDK = 1

	conf.check_tool('stellart')
	conf.check_stellart()

	conf.check_tool('pbasdk')

	conf.check_tool('socket')
	conf.check_tool('CurlClient')

	conf.check_tool('openssl')
	conf.check_tool('libxml2')
	conf.check_tool('TimeTool')
#	conf.check_tool('plugin')
	conf.check_tool('xmlpacket')
        conf.check_tool('RapJson')

def build(bld):
	stlrt.correct_system_env(bld)
	stlrt.correct_build_jobs(bld)

	cnttask = LISA.new_task_gen(bld,
		features	= 'lsaprogram',
		source		= APPNAME + '.lsa',
		bodies		= [ 'bodies.cc' ],
		internals	= [ 'internals.cc' ],
		uselib		= [ 'LIBXML2', 'STELLART_TM', 'STELLART_XMLSCREEN', 'PBASDK', 'TIMETOOL', 'PLUGIN', 'XMLPACKET', 'RAPJSON', 'CURLCLIENT'],
	)
        bld.install_files('${INSTALL_ROOT}/etc/ssm.conf.d', 'etc/ssm.conf.d/containers-list.dist.*');
	if Options.options.rpm: rpm(bld)

def rpm(bld):
	plug_buildroot = os.path.join(os.sep, 'tmp',
		'bm-payment-plugin-' + APPNAME.lower() + '-' + VERSION + '-root')
	if os.path.exists(plug_buildroot): shutil.rmtree(plug_buildroot)

	## prepare directories structure for packaging
	buildroot = os.path.join(plug_buildroot, 'usr', 'local')
	os.makedirs(buildroot)

	os.unlink(os.path.join(bld.env['INSTALL_ROOT'], 'conf', 'wnd', 'customization.links'))
	shutil.move(bld.env['INSTALL_ROOT'], os.path.join(buildroot, 'bm'))

	# Tag is like bm5_0_000_213
	# but we should require 5.0.0-213
	reg = re.compile(r'bm(\d)_(\d+)_0*(\d+)_(\d+)')
	m = reg.match(bld.env.PBASDK_VERSION)
	if m == None:
		print("Failed to define PBA version to specify in RPM requirements");
		exit(-1)
	grp = m.group
	pba_major = grp(1)
	pba_minor = grp(2)
	pba_maintenance = grp(3)
	pba_build = grp(4)

	args = [ 'rpmbuild', '--buildroot', plug_buildroot, '--target' ]
	args += [ 'x86_64' ]

	for x in [
		'title %s' % APPTITLE,
		'version %s' % VERSION,
		'release %s' % RELEASE,
		'container %s' % APPNAME,
		'lwcontainer %s' % APPNAME.lower(),
		'pba_major %s' % pba_major,
		'pba_minor %s' % pba_minor,
		'pba_maintenance %s' % pba_maintenance,
		'pba_build %s' % pba_build,
		'bindir_suffix %s' % (bld.env.params['BINDIR_SUFFIX'] if bld.env.params['BINDIR_SUFFIX'] else '%{?__none__}'),
		'libdir_suffix %s' % (bld.env.params['LIBDIR_SUFFIX'] if bld.env.params['LIBDIR_SUFFIX'] else '%{?__none__}'),
	]:
		args += ['--define', x]
	args += ['-bb', 'plugin.spec']

	bld.exec_command(args, **{ 'env': os.environ, 'cwd': bld.path.abspath() })
